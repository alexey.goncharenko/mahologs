<?php

namespace CLog;

use \Monolog\Processor;

/**
 * Class Logger
 */
class Logger extends \Monolog\Logger
{
    const BOOL = 'bool';
    const DOUBLE = 'double';
    const INT = 'int';
    const STRING = 'string';

    /** Maximum field size in byte */
    const MAX_LENGTH = 30000;

    /** @var array Definition of context fields to use */
    protected $contextFieldMap = [
        'code'            => self::INT,
        'curlError'       => self::STRING,
        'location'        => self::STRING,
        'message'         => self::STRING,
        'requestBody'     => self::STRING,
        'requestDuration' => self::DOUBLE,
        'requestHeaders'  => self::STRING,
        'requestHostname' => self::STRING,
        'requestMethod'   => self::STRING,
        'requestQuery'    => self::STRING,
        'responseBody'    => self::STRING,
        'responseHeaders' => self::STRING,
        'statusCode'      => self::INT,
        'timeSinceStart'  => self::DOUBLE,
        'trace'           => self::STRING,
    ];

    /** @var array The global logging context which always gets added to a log message */
    protected $globalContext = [];

    /** @var bool Enables checking of existence of context field definition. If a definition is missing on strictMode an RuntimeException will be thrown */
    protected $strictMode = false;

    /**
     * @param string                              $name            The logging channel
     * @param \Monolog\Handler\HandlerInterface[] $handlers        Optional stack of handlers, the first one in the array is called first, etc.
     * @param callable[]                          $processors      Optional array of processors
     * @param array                               $contextFieldMap Optional context field definition
     */
    public function __construct($name = 'checkout', array $handlers = [], array $processors = [], $contextFieldMap = [])
    {
        parent::__construct($name, $handlers, $processors);

        $this->addContextFieldMap($contextFieldMap);

        $this->pushProcessor(new Processor\MemoryUsageProcessor());
        $this->pushProcessor(new Processor\MemoryPeakUsageProcessor());
        $this->pushProcessor(
            function (array $record) {
                $record['timeSinceStart'] = round(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 5);

                return $record;
            }
        );
    }

    /**
     * Sets the definition of context fields to use
     * @param array $contextFieldMap
     */
    public function addContextFieldMap($contextFieldMap)
    {
        $this->contextFieldMap = array_merge($this->contextFieldMap, $contextFieldMap);
    }

    /**
     * @return array
     */
    public function getContextFieldMap()
    {
        return $this->contextFieldMap;
    }

    /**
     * Merges the given array into the global logging context
     * @param array $context
     */
    public function addArrayToContext(array $context)
    {
        $this->globalContext = array_merge($this->globalContext, $context);
    }

    /**
     * Removes a given key from the global logging context
     *
     * @param string $key
     */
    public function removeKeyFromContext($key)
    {
        unset($this->globalContext[$key]);
    }

    /**
     * Adds the value to the global context with the given name
     * @param string $name
     * @param mixed  $value
     */
    public function addToContext($name, $value)
    {
        $this->addArrayToContext([$name => $value]);
    }

    /**
     * Adds the context the loggableInterface provides to the global logging context
     * @param LoggableInterface $loggable
     */
    public function addLoggableToContext(LoggableInterface $loggable)
    {
        $this->addArrayToContext($loggable->getLogContext());
    }

    /**
     * Returns if the strictMode is enabled
     * @return boolean
     */
    public function isStrictMode()
    {
        return $this->strictMode;
    }

    /**
     * Enables the checking if a context field exists in the definition.
     * If not and strictMode is enabled an RuntimeException will be thrown.
     */
    public function enableStrictMode()
    {
        $this->strictMode = true;
    }

    /**
     * Adds a log record.
     * @param  integer $level   The logging level
     * @param  string  $message The log message
     * @param  array   $context The log context
     * @return Boolean Whether the record has been processed
     */
    public function addRecord($level, $message, array $context = [])
    {
        $configuration = include __DIR__.'/config.php';

        foreach ($configuration['excludeLevels'] as $excludeLevel) {
            if ($level == $excludeLevel) return;
        }

        if ($configuration['showOnlyMessages']) {
            foreach ($configuration['showOnlyMessages'] as $showOnlyMessage) {
                if (strpos(strtolower($message), strtolower($showOnlyMessage)) === false) {
                    return;
                }
            }
        } else {
            foreach ($configuration['excludeMessages'] as $excludeMessage) {
                if (strpos(strtolower($message), strtolower($excludeMessage)) !== false) {
                    return;
                }
            }
        }

        $showContext = true;
        if ($configuration['showContextOnlyForMessages']) {
            $showContext = false;
            foreach ($configuration['showContextOnlyForMessages'] as $showContextOnlyForMessage) {
                if (strpos(strtolower($message), strtolower($showContextOnlyForMessage)) !== false) {
                    $showContext = true;
                }
            }
        }

        if (!$showContext && $configuration['showContextForLevels']) {
            $showContext = in_array($level, $configuration['showContextForLevels']);
        }

        if ($message instanceof \Throwable) {
            return $this->logException($message, $level, $context);
        }

        $context = array_merge($this->globalContext, $context);
        $message = $this->castValueByDefinition('message', $message);
        $context = $showContext ? self::processContext($context) : [];
        return parent::addRecord($level, $message, $context);
    }

    /**
     * Casts the value to a json string
     * @param $value
     * @return string
     */
    protected function castToJsonString($value)
    {
        if (is_null($value)) {
            return '';
        }

        if (is_resource($value)) {
            return (string)$value;
        }

        if (is_object($value)) {
            $value = (array)$value;
        }

        //We need to convert resources to string since json_encode cannot handle them correctly
        array_walk_recursive($value, function (&$subValue, $subKey) {
            if (is_resource($subValue)) {
                $subValue = (string)$subValue;
            }
        });

        return json_encode($value);
    }

    /**
     * Casts a value by its definition in the contextFieldMap
     * If the value is a string and its longer than 30k bytes we'll trim it to 30k bytes
     * @param $name
     * @param $value
     * @return array|float|int|string
     */
    protected function castValueByDefinition($name, $value)
    {
        if (!array_key_exists($name, $this->contextFieldMap) && $this->strictMode) {
            $this->removeKeyFromContext($name);
            throw new \RuntimeException("Field '$name' does not exist in context-field-definition.");
        }

        $targetType = isset($this->contextFieldMap[$name]) ? $this->contextFieldMap[$name] : null;
        switch (true) {
            case $targetType === self::INT:
                $value = (int)$value;
                break;
            case $targetType === self::DOUBLE:
                $value = (double)$value;
                break;
            case $targetType === self::BOOL:
                $value = (int)((bool)$value);
                break;
            case $targetType === self::STRING:
                if (!is_scalar($value)) {
                    $value = $this->castToJsonString($value);
                }
                $value = (string)$value;
                break;
            default:
                if (!is_scalar($value)) {
                    $value = $this->castToJsonString($value);
                }
                break;
        }

        if (gettype($value) == 'string' && strlen($value) > self::MAX_LENGTH) {
            $value = substr($value, 0, self::MAX_LENGTH);
        }

        return $value;
    }

    /**
     * @param array $context The context data which should be trimmed
     * @return array The altered context data
     */
    protected function processContext($context)
    {
        array_walk($context, function (&$value, $key) {
            $value = $this->castValueByDefinition($key, $value);
        });

        return $context;
    }

    /**
     * Logs a request & response
     * @param string $name
     * @param array  $request
     * @param array  $response
     * @param array  $allowedStatusCodes
     * @param array  $context
     * @return bool
     */
    public function logRequestResponse($name, $request, &$response, $allowedStatusCodes = [200], $context = [])
    {
        $message = "[{$name}] Api call ";

        $isAllowedStatusCode = in_array($response['status'], $allowedStatusCodes);
        $hasCurlError = isset($response['curl']) ? $response['curl']['error'] != '' : false;

        if (!$isAllowedStatusCode || $hasCurlError) {
            $level = Logger::ERROR;
            $message .= 'failed';
        } else {
            $level = Logger::INFO;
            $message .= 'succeeded';
        }

        $context = array_merge($this->getRequestResponseContext($request, $response), $context);

        return $this->addRecord($level, $message, $context);
    }

    /**
     * Logs an exception.
     * Else: Critical
     * Optional add the loglevel to method call, to force this specific log level
     * @param \Throwable $exception
     * @param int        $logLevel Optional LogLevel definition
     * @param array      $context additional context information
     * @return bool
     */
    public function logException(\Throwable $exception, $logLevel = null, $context = [])
    {
        $context = array_merge(
            [
                'trace'    => $exception->getTraceAsString(),
                'location' => $exception->getFile() . ' (Line: ' . $exception->getLine() . ')',
                'code'     => $exception->getCode(),
            ],
            $context
        );

        switch (true) {
            case $exception instanceof LoggableExceptionInterface:
                $level = $exception->getLogLevel();
                $context = array_merge($context, $exception->getLogContext());
                break;
            default:
                $level = Logger::CRITICAL;
                break;
        }

        // override log level in case the parameter is set and valid
        if ($logLevel !== null && array_key_exists($logLevel, Logger::$levels)) {
            $level = $logLevel;
        }

        $message = $exception->getMessage();
        if (empty(trim($message))) {
            $message = get_class($exception);
        }

        return $this->addRecord($level, $message, $context);
    }

    /**
     * Returns an array with all important fields from the request & response object
     * @param array $request
     * @param array $response
     * @return array
     */
    public function getRequestResponseContext($request, &$response)
    {
        $body = $response['body'];
        if (is_resource($body)) {
            $memoryStream = fopen('php://memory', 'wb+');
            $bytesWritten = stream_copy_to_stream($body, $memoryStream);
            rewind($memoryStream);
            rewind($response['body']);
            $body = stream_get_contents($memoryStream);
        }

        $context = [
            'statusCode'      => $response['status'],
            'requestHeaders'  => isset($request['headers']) ? json_encode($request['headers']) : '',
            'requestBody'     => isset($request['body']) ? $request['body'] : '',
            'requestHostname' => $request['headers']['host'][0],
            'requestQuery'    => $request['uri'],
            'requestMethod'   => $request['http_method'],
            'responseBody'    => $body,
            'responseHeaders' => $response['headers'],
            'requestDuration' => (float)$response['transfer_stats']['total_time'],
            'curlError'       => isset($response['curl']) ? $response['curl']['error'] : '',
        ];

        return $context;
    }
}
