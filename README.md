# MahoLogs
This tool will help to make work with checkout logs more comfortable.
At this moment it works only with COFE logs.

### IMPORTANT
- Don't forget that filtering logs and switching off log's context can hide some important details from you. 
- Always check original logs for final tests.

### Set up
- `git clone`
- `cd mahologs`
- `chmod +x run`
- `cp config.php.example config.php`
- `cp maho-config.php.example maho-config.php`
- Open maho-config.php and set correct path

### Configuring
- Open config.php and set desired options
- `./run`

### Recover original logs
- Delete vendor folder
- Reinstall it via `composer install`

### Typical use cases
#### Hide all debug messages 
```php
'excludeLevels' => [ // Do not show this level in logs
    static::DEBUG,
],
```

#### Show only messages which includes "Arvato"
```php
'showOnlyMessages' => [ // If it's specified then `excludeMessages` will be ignored
    'arvato',
],
```

#### Do not show context for any log message except Computop calls and Errors
```php
'showContextOnlyForMessages' => [ // If it's specified other messages will be logged as single line
    'computop',
],
'showContextForLevels' => [ // Despite of `showContextOnlyForMessages` is specified, show context for the next levels
    static::CRITICAL,
    static::ERROR,
],
```

#### Exclude some log messages
```php
'excludeMessages' => [ // Do not show this messages which includes this parts in logs
    'ResourceLock',
    'RuleEngine',
],
```
